//This example code is in the Public Domain (or CC0 licensed, at your option.)
//By Victor Tchistiak - 2019
//
//This example demostrates master mode bluetooth connection and pin 
//it creates a bridge between Serial and Classical Bluetooth (SPP)
//this is an extention of the SerialToSerialBT example by Evandro Copercini - 2018
//
#include "BluetoothSerial.h"

#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif

BluetoothSerial SerialBT;
#include <ArduinoJson.h>

// compute the required size
//const size_t CAPACITY = JSON_ARRAY_SIZE(1200);
StaticJsonDocument<900> doc;
//StaticJsonDocument<900> start;
//JsonArray array = doc.createNestedArray("array");

byte bytearray[1];
void setup() {
  Serial.begin(115200);

  SerialBT.begin("ESP32test"); //Bluetooth device name
  Serial.println("The device started, now you can pair it with bluetooth!");
}
int a;
void loop() {

/* doc["CONFIG"]["FRQ"] = 868.50;
 doc["CONFIG"]["DBM"] = 22;
 doc["CONFIG"]["PRSHT"] = 600;
 doc["CONFIG"]["FLSH_CLEAN"] = 0;
 doc["CONFIG"]["STATUS"] = 0;
 doc["CONFIG"]["UNIT"] = "METRIC";*/
/* 
 doc.clear();
 doc["ROCKET"]["H3LIS331"]["ACCEL_X"]=2.01;
 doc["ROCKET"]["H3LIS331"]["ACCEL_Y"]=3.97;
 doc["ROCKET"]["H3LIS331"]["ACCEL_Z"]=1.26;

 a++;
 if(a>=1000)a=0;
 doc["ROCKET"]["MS5611"]["ALTITUDE"]=a;
 doc["ROCKET"]["MS5611"]["TEMP"]=32.26;
 doc["ROCKET"]["MS5611"]["SPEED"]=50.26;

 doc["ROCKET"]["FLIGHT"]["ID"]=1;
 doc["ROCKET"]["FLIGHT"]["MODE"]=0;

 doc["ROCKET"]["BNO055"]["ACCEL_X"]=2.23;
 doc["ROCKET"]["BNO055"]["ACCEL_Y"]=3.25;
 doc["ROCKET"]["BNO055"]["ACCEL_Z"]=5.5;

 doc["ROCKET"]["BNO055"]["GYRO_X"]=100;
 doc["ROCKET"]["BNO055"]["GYRO_Y"]=200;
 doc["ROCKET"]["BNO055"]["GYRO_Z"]=300;

 doc["ROCKET"]["BNO055"]["MAG_X"]=25.5;
 doc["ROCKET"]["BNO055"]["MAG_Y"]=30.26;
 doc["ROCKET"]["BNO055"]["MAG_Z"]=-20.26;

 doc["ROCKET"]["BNO055"]["YAW"]=45;
 doc["ROCKET"]["BNO055"]["PITCH"]=90;
 doc["ROCKET"]["BNO055"]["ROLL"]=180;

 doc["ROCKET"]["GPS"]["LAT"]=40.123456;
 doc["ROCKET"]["GPS"]["LONG"]=29.123456;
 doc["ROCKET"]["GPS"]["SAT"]=12;
 doc["ROCKET"]["GPS"]["SPEED"]=150;

 doc["ROCKET"]["BATTERY"]["ROCKET"] = 3.7;
 doc["ROCKET"]["BATTERY"]["GS"] = 4.2;

 doc["ROCKET"]["SD_FREE"] = 30;


 doc["ROCKET"]["LORA"]["RSSI"] = -39.01;

 
 doc["ROCKET"]["PYRO"]["APOGEE"]=1;
 doc["ROCKET"]["PYRO"]["MAIN"]=1;*/


 

//Serial.println(doc.memoryUsage()); 

     if (SerialBT.available() ) {
     
      DeserializationError error = deserializeJson(doc, SerialBT);
       serializeJsonPretty(doc, Serial);
       if( doc["CONFIG"]["RQ"]==true)a=1;
        
          if (error) {
            Serial.print(F("deserializeJson() failed: "));
            Serial.println(error.f_str());
            return;
     }
  }
  if(a==1){
    Serial.println("A = 1");
 doc.clear();
 String s1 ={"!"};
s1.getBytes(bytearray, 2);

SerialBT.write(bytearray[0]);
delay(20);
 doc["CONFIG"]["FRQ"] = 868.50;
 doc["CONFIG"]["DBM"] = 22;
 doc["CONFIG"]["PRSHT"] = 600;
 doc["CONFIG"]["FLSH_CLEAN"] = 0;
 doc["CONFIG"]["STATUS"] = 0;
 doc["CONFIG"]["UNIT"] = "METRIC";
 serializeJson(doc, SerialBT);
 a=0;
    
    }
 /*array.add(123);
  array.add("hello");
  array.add("world");
  array.add("cpp");
  array.add("secrets");
  array.add(".com");*/
/* 
String s1 ={"!"};
s1.getBytes(bytearray, 2);

SerialBT.write(bytearray[0]);
delay(20);
serializeJson(doc, SerialBT);
delay(60);*/
/*if(a>=100){
  
   delay(30);
   SerialBT.write(bytearray[0]);
   delay(30);
   doc.clear();
   
 doc["FLIGHT_STATISTICS"]["CRC_ERR"] = 1;// istatistğe geç

 doc["FLIGHT_STATISTICS"]["MAX_ALTITUDE"]=3000;
 doc["FLIGHT_STATISTICS"]["MAX_SPEED"]=163;

 doc["FLIGHT_STATISTICS"]["RISE_ACCEL_X"]=12.5;
 doc["FLIGHT_STATISTICS"]["RISE_ACCEL_Y"]=15.2;
 doc["FLIGHT_STATISTICS"]["RISE_ACCEL_Z"]=8.8;

 doc["FLIGHT_STATISTICS"]["FALL_ACCEL_X"]=12.5;
 doc["FLIGHT_STATISTICS"]["FALL_ACCEL_Y"]=15.2;
 doc["FLIGHT_STATISTICS"]["FALL_ACCEL_Z"]=8.8;
 
 doc["FLIGHT_STATISTICS"]["MAX_ACCEL_X"]=12.5;
 doc["FLIGHT_STATISTICS"]["MAX_ACCEL_Y"]=15.2;
 doc["FLIGHT_STATISTICS"]["MAX_ACCEL_Z"]=8.8;
 
 doc["FLIGHT_STATISTICS"]["MAX_SATALITE"]=12;
 doc["FLIGHT_STATISTICS"]["ENGINE_BURN_TIME"]=2;
 doc["FLIGHT_STATISTICS"]["APOGEE_TIME"]=19;
 doc["FLIGHT_STATISTICS"]["APOGEE_FALL_SPEED"]=25;
 doc["FLIGHT_STATISTICS"]["MAIN_FALL_SPEED"]=9;
 doc["FLIGHT_STATISTICS"]["DESCENT_TIME"]=45;
 doc["FLIGHT_STATISTICS"]["PYRO_BURN_TIME"]=0.12;
 doc["FLIGHT_STATISTICS"]["FLIGHT_TIME"]=64;
 doc["FLIGHT_STATISTICS"]["MACH_LOCK"]=0;
 doc["FLIGHT_STATISTICS"]["FLSH_FREE"]=65;
 serializeJson(doc, SerialBT);
  }*/

//serializeJsonPretty(doc, Serial);

/*String s1 ="yunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyunusyun";
//byte bytearray[sizeof(s1)];
s1.getBytes(bytearray, 330);
SerialBT.write(bytearray,330);*/

/*  if (Serial.available()) {
    SerialBT.write(Serial.read());
  }
  if (SerialBT.available()) {
    Serial.write(SerialBT.read());
  }*/
  
}
